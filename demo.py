import clip
import torch
import glob
import os
from PIL import Image, ImageDraw
import numpy as np


class TokenAttnDrawing:
    def __init__(self, ):
        pass
    
    @staticmethod
    def visualize(img, bbox1, bbox2, top_idx1, top_idx2, save_path, color1="red", color2="green", patch_num=14):
        img_draw = ImageDraw.Draw(img)
        img_draw.rectangle([(bbox1[0]-1,bbox1[1]-1),(bbox1[2],bbox1[3])], outline =color1)
        img_draw.rectangle([(bbox2[0]-1,bbox2[1]-1),(bbox2[2],bbox2[3])], outline =color2)
        
        bg = np.zeros((patch_num, patch_num)).reshape(-1)
        fg1, fg2 = bg.copy(), bg.copy()

        fg1[top_idx1.cpu()] = 1
        fg2[top_idx2.cpu()] = 1
        mask1 = fg1.reshape((patch_num, patch_num))
        mask2 = fg1.reshape((patch_num, patch_num))

        fg_c1 = np.array([[[127,0,0,125]]])
        fg_c2 = np.array([[[0,127,0,125]]])
        fg1 = fg_c1 * mask1[:,:,np.newaxis]
        fg2 = fg_c2 * mask2[:,:,np.newaxis]

        fg1 = Image.fromarray(fg1.astype(np.uint8))
        fg2 = Image.fromarray(fg2.astype(np.uint8))
        
        img.putalpha(255)

        fg1_resize = fg1.resize((bbox1[2]-bbox1[0], bbox1[3]-bbox1[1]), Image.NEAREST)
        fg2_resize = fg2.resize((bbox2[2]-bbox2[0], bbox2[3]-bbox2[1]), Image.NEAREST)

        mask1_blend = mask1.copy()
        mask1_blend[mask1_blend==1] = 180
        mask1_blend[mask1_blend==0] = 255
        
        mask2_blend = mask2.copy()
        mask2_blend[mask2_blend==1] = 180
        mask2_blend[mask2_blend==0] = 255
        
        mask1_resize = Image.fromarray(mask1_blend).resize((bbox1[2]-bbox1[0], bbox1[3]-bbox1[1]), Image.NEAREST).convert('L')
        mask2_resize = Image.fromarray(mask2_blend).resize((bbox2[2]-bbox2[0], bbox2[3]-bbox2[1]), Image.NEAREST).convert('L')


        fg1_resize_comp = Image.composite(img.crop(bbox1), fg1_resize, mask=mask1_resize)
        fg2_resize_comp = Image.composite(img.crop(bbox2), fg2_resize, mask=mask2_resize)

        img.paste(fg1_resize_comp, (bbox1[0],bbox1[1]))
        img.paste(fg2_resize_comp, (bbox2[0],bbox2[1]))

        os.makedirs(os.path.dirname(save_path), exist_ok=True)
        img.save(save_path)
        # import pdb; pdb.set_trace();


class TokenFeatureExtractor:
    def __init__(self,):
        pass

    def forward(self, model, x):
        x = model.conv1(x)  # shape = [*, width, grid, grid]
        x = x.reshape(x.shape[0], x.shape[1], -1)  # shape = [*, width, grid ** 2]
        x = x.permute(0, 2, 1)  # shape = [*, grid ** 2, width]
        x = torch.cat([model.class_embedding.to(x.dtype) + torch.zeros(x.shape[0], 1, x.shape[-1], dtype=x.dtype, device=x.device), x], dim=1)  # shape = [*, grid ** 2 + 1, width]
        x = x + model.positional_embedding.to(x.dtype)
        x = model.ln_pre(x)

        x = x.permute(1, 0, 2)  # NLD -> LND
        x = model.transformer(x)
        x = x.permute(1, 0, 2)  # LND -> NLD

        # x = model.ln_post(x[:, 0, :])

        return x[0,0:1,:], x[0,1:,:]

    @staticmethod
    def top_idx_by_cosine(x_tokens, x_cls, ratio=0.25):
        scores = torch.cosine_similarity(x_tokens, x_cls)
        indices = torch.sort(scores, dim=0).indices
        top_idx = indices[-int(len(indices)*0.25):]
        return top_idx


def prepare_data(img_path):
    img_pil = Image.open(img_path)
    
    image_origin_shape = np.array([1980, 1400])
    bbox_origin1 = np.array([[290, 154,],[1300,815]]).astype(np.float)
    bbox_origin2 = np.array([[851, 457,],[1854,1230]]).astype(np.float)
    bbox_origin1[:,0], bbox_origin2[:,0] = bbox_origin1[:,0]/image_origin_shape[0], bbox_origin2[:,0]/image_origin_shape[0] 
    bbox_origin1[:,1], bbox_origin2[:,1] = bbox_origin1[:,1]/image_origin_shape[1], bbox_origin2[:,1]/image_origin_shape[1]

    w, h = img_pil.size[0], img_pil.size[1]
    bbox1 = (int(bbox_origin1[0,0]*w), int(bbox_origin1[0,1]*h), int(bbox_origin1[1,0]*w), int(bbox_origin1[1,1]*h))
    bbox2 = (int(bbox_origin2[0,0]*w), int(bbox_origin2[0,1]*h), int(bbox_origin2[1,0]*w), int(bbox_origin2[1,1]*h))

    # print(bbox1, bbox2)
    return img_pil, bbox1, bbox2


def main():
    img_paths = glob.glob(os.path.join('imgs', '*.png'))
    
    img_path = img_paths[0]
    img_pil, bbox1, bbox2 = prepare_data(img_path)
    
    device = torch.device('cuda')
    model, preprocess = clip.load("ViT-B/16", device=device)
    crop1 = preprocess(img_pil.crop(bbox1)).unsqueeze(0).to(device).half()
    crop2 = preprocess(img_pil.crop(bbox2)).unsqueeze(0).to(device).half()
    
    token_feature_extractor = TokenFeatureExtractor()
    x_cls1, x_tokens1 = token_feature_extractor.forward(model.visual, crop1)
    x_cls2, x_tokens2 = token_feature_extractor.forward(model.visual, crop2)

    top_idx1 = token_feature_extractor.top_idx_by_cosine(x_tokens1, x_cls2.expand(*x_tokens1.shape))
    top_idx2 = token_feature_extractor.top_idx_by_cosine(x_tokens2, x_cls1.expand(*x_tokens2.shape))

    save_path = os.path.join('runs/output_imgs', os.path.basename(img_path))
    TokenAttnDrawing.visualize(img_pil, bbox1, bbox2, top_idx1, top_idx2, save_path=save_path)


if __name__ == '__main__':
    main()
